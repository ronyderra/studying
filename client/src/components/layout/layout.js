import React from "react";
import "./layout.css";
import Navbar from '../navbar/navbar'

export default function Layout() {
  return (
    <div className="layout">
      <header className="header"><Navbar/></header>
      <main className="main">main</main>
      <footer className="footer">footer</footer>
    </div>
  );
}
