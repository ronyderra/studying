import React from "react";
import "./navbar.css";
import {
  FcHome,
  FcAbout,
  FcNews,
  FcPanorama,
  FcCustomerSupport,
  FcAreaChart,
} from "react-icons/fc";
import { IconContext } from "react-icons";
import { Col, Row, Container } from "react-bootstrap";

export default function Navbar(props) {
  return (
    <Container>
      <Row>
        <Col md={4}>
          logo
        </Col>
        <Col md={{ span: 4, offset: 8  }}>
            <li>
              <IconContext.Provider value={{ size: "5em" }}>
                <a className="homeIcon" href="#home">
                  <FcHome />
                </a>
              </IconContext.Provider>
            </li>
            <li>
              <IconContext.Provider value={{ size: "5em" }}>
                <a href="#news">
                  <FcAbout />
                </a>
              </IconContext.Provider>
            </li>
            <li>
              <IconContext.Provider value={{ size: "5em" }}>
                <a href="#contact">
                  <FcNews />
                </a>
              </IconContext.Provider>
            </li>
            <li>
              <IconContext.Provider value={{ size: "5em" }}>
                <a href="#about">
                  <FcPanorama />
                </a>
              </IconContext.Provider>
            </li>
            <li>
              <IconContext.Provider value={{ size: "5em" }}>
                <a href="#about">
                  <FcCustomerSupport />
                </a>
              </IconContext.Provider>
            </li>
            <li>
              <IconContext.Provider value={{ size: "5em" }}>
                <a href="#about">
                  <FcAreaChart />
                </a>
              </IconContext.Provider>
            </li>
        </Col>
      </Row>
    </Container>
  );
}
